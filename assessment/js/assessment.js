 $(document).ready(function() {
     var i = 1,j=1;
     if (JSON.parse(localStorage.getItem('list')) == null) {
         localStorage.setItem('list', $("#hrdata").text());
     } else {
         var retrievedObject = localStorage.getItem('list');
         var data = JSON.parse(retrievedObject);
     }
     $(document).on("click", "#addbut", function() {

         data.applicants.push({
             "appid":""+i,
             "name": $("#name").val(),
             "fname": $("#fname").val(),
             "dob": $("#dob").val(),
             "mail": $("#mail").val(),
             "phno": $("#phno").val(),
             "degree": $("#deg").val(),
             "skill": $("#ski").val(),
             "experience": $("#expe").val(),
             "percentage": $("#per").val()

         })
         i++;
         localStorage.setItem('list', JSON.stringify(data));
         $("form").trigger("reset").find("input","select").removeClass("is-valid");
     })
     $(document).on("click", ".view", function() {
         $("#display").empty();
         $("#display").append('<table class="table tab"><thead class="thead-dark"><tr><th>ID</th><th>NAME</th><th>FATHER NAME</th><th>DATE OF BIRTH</th><th>EMAIL</th><th>PHONE NO</th><th>DEGREE</th>'+
            '<th>SKILLS</th><th>EXPERIENCE</th><th>PERCENTAGE</th><th></th></tr></thead><tbody class="viewbody"></tbody></table>');
         for (var i = 0; i < data.applicants.length; i++) {
             $(".viewbody").append('<tr>' +
                 '<td>' + data.applicants[i].appid + '</td>' +
                 '<td>' + data.applicants[i].name + '</td>' +
                 '<td>' + data.applicants[i].fname + '</td>' +

                 '<td>' + data.applicants[i].dob + '</td>' +
                 '<td>' + data.applicants[i].mail + '</td>' +
                 '<td>' + data.applicants[i].phno + '</td>' +
                 '<td>' + data.applicants[i].degree + '</td>' +
                 '<td>' + data.applicants[i].skill + '</td>' +
                 '<td>' + data.applicants[i].experience + '</td>' +
                 '<td>' + data.applicants[i].percentage + '</td>' +
                 '<td><button typr="button" class="sel btn btn-info"  id="' +data.applicants[i].appid+ '">select</button></td>' +
                 '</tr>');
         }
     })

     $(document).on("click", ".sel", function() {
         var b = $(this).attr("id");
         var a = b - 1;
         $(this).prop("disabled",true).text("selected");
         data.selectlis.push({
             "name": data.applicants[a].name,
             "fname": data.applicants[a].fname,
             "appid": data.applicants[a].appid,
             "dob": data.applicants[a].dob,
             "mail": data.applicants[a].mail,
             "phno": data.applicants[a].phno,
             "degree": data.applicants[a].degree,
             "skill": data.applicants[a].skill,
             "experience": data.applicants[a].experience,
             "percentage": data.applicants[a].percentage

         })
         localStorage.setItem('list', JSON.stringify(data));
     })
     $(document).on("click", ".short", function() {
        $("#display").empty();
         $("#display").append('<table class="table tab"><thead class="thead-dark"><tr><th>ID</th><th>NAME</th><th>FATHER NAME</th><th>DATE OF BIRTH</th><th>EMAIL</th><th>PHONE NO</th><th>DEGREE</th>'+
            '<th>SKILLS</th><th>EXPERIENCE</th><th>PERCENTAGE</th><th></th></tr></thead><tbody class="tbody"></tbody></table>');
         for (var i = 0; i < data.selectlis.length; i++) {
             $(".tbody").append('<tr>' +
                 '<td>' + data.selectlis[i].appid + '</td>' +
                 '<td>' + data.selectlis[i].name + '</td>' +
                 '<td>' + data.selectlis[i].fname + '</td>' +
                 '<td>' + data.selectlis[i].dob + '</td>' +
                 '<td>' + data.selectlis[i].mail + '</td>' +
                 '<td>' + data.selectlis[i].phno + '</td>' +
                 '<td>' + data.selectlis[i].degree + '</td>' +
                 '<td>' + data.selectlis[i].skill + '</td>' +
                 '<td>' + data.selectlis[i].experience + '</td>' +
                 '<td>' + data.selectlis[i].percentage + '</td>' +
                 '<td><button type="button" id="' + data.selectlis[i].appid + '" class="btn btn-info inter" data-toggle="modal" data-target="#myInter">schedule</button></td>' +
                 '</tr>');
         }
     })

     $(document).on("click", "#send", function() {
             d.hrmsgs.push({
                 "recipient": $("#rname").val(),
                 "message": $("#msg").val()
             })
             localStorage.setItem('list', JSON.stringify(data));
         })
         // $(document).on("click", "#del", function() {
         //     var a = $("#appli").val();
         //     console.log(a);
         //     delete data.applicants[a];
         //     localStorage.setItem('applican', JSON.stringify(data));
         // })
     $(document).on("click", "#jd", function() {
          $("#display").empty();
         $("#display").append('<table class="table tab"><thead class="thead-dark"><tr><th>ID</th><th>TITLE</th><th>PERCENTAGE</th><th>EXPERIENCE</th><th>OPENINGS</th><th>SKILLS</th>'+
            '</tr></thead><tbody class="descbody"></tbody></table>');
         for (var i = 0; i < data.Description.length; i++) {

             $(".descbody").append('<tr>' +
                 '<td>' + data.Description[i].id + '</td>' +
                 '<td>' + data.Description[i].title + '</td>' +
                 '<td>' + data.Description[i].percentage + '</td>' +
                 '<td>' + data.Description[i].experience + '</td>' +
                 '<td>' + data.Description[i].openings + '</td>' +
                 '<td>' + data.Description[i].skills + '</td>' +
                 '</tr>');
         }
     })
     $(document).on("click", ".inter", function() {
         var b = $(this).attr("id");
         var a=b-1;
         $(this).prop("disabled",true).text("scheduled");
          $("#display1").empty();
         $("#display1").append('<table class="table tab"><thead class="thead-dark"><tr><th>ID</th><th>NAME</th><th>EMAIL</th></tr></thead><tbody class="disbody"></tbody></table>')
         $(".disbody").append('<tr>' +
             '<td class="disid">' + data.applicants[a].appid + '</td>' +
             '<td>' + data.applicants[a].name + '</td>' +
             // '<td>' + data.applicants[a].fname + '</td>' +
             // '<td>' + data.applicants[a].dob + '</td>' +
             '<td>' + data.applicants[a].mail + '</td>' +
             // '<td>' + data.applicants[a].phno + '</td>' +
             // '<td>' + data.applicants[a].degree + '</td>' +
             // '<td>' + data.applicants[a].skill + '</td>' +
             // '<td>' + data.applicants[a].experience + '</td>' +
             // '<td>' + data.applicants[a].percentage + '</td>' +
             '</tr>');

         localStorage.setItem('list', JSON.stringify(data));
     })
     $(document).on("click", "#interbut", function() {

         var b = $("#display1").find(".disid").text();
         console.log(b);
         var a=b-1;
         console.log(a);
         data.schedule.push({
             "name": data.applicants[a].name,
             "fname": data.applicants[a].fname,
             "appid": data.applicants[a].appid,
             "dob": data.applicants[a].dob,
             "mail": data.applicants[a].mail,
             "phno": data.applicants[a].phno,
             "degree": data.applicants[a].degree,
             "skill": data.applicants[a].skill,
             "experience": data.applicants[a].experience,
             "percentage": data.applicants[a].percentage,
             "dateofinterview": $("#interdate").val(),
             "interviewer": $("#intertech").val()
         })

         localStorage.setItem('list', JSON.stringify(data));
     })
     $(document).on("click", ".int", function() {
          $("#display").empty();
         $("#display").append('<table class="table tab"><thead class="thead-dark"><tr><th>ID</th><th>NAME</th><th>FATHER NAME</th><th>DATE OF BIRTH</th><th>EMAIL</th><th>PHONE NO</th><th>DEGREE</th>'+
            '<th>SKILLS</th><th>EXPERIENCE</th><th>PERCENTAGE</th><th>DATE OF INTERVIEW</th><th>INTERVIEWER</th></tr></thead><tbody class="schbody"></tbody></table>');
         for (var i = 0; i < data.schedule.length; i++) {
             $(".schbody").append('<tr>' +
                 '<td>' + data.schedule[i].appid + '</td>' +
                 '<td>' + data.schedule[i].name + '</td>' +
                 '<td>' + data.schedule[i].fname + '</td>' +
                 '<td>' + data.schedule[i].dob + '</td>' +
                 '<td>' + data.schedule[i].mail + '</td>' +
                 '<td>' + data.schedule[i].phno + '</td>' +
                 '<td>' + data.schedule[i].degree + '</td>' +
                 '<td>' + data.schedule[i].skill + '</td>' +
                 '<td>' + data.schedule[i].experience + '</td>' +
                 '<td>' + data.schedule[i].percentage + '</td>' +
                 '<td>' + data.schedule[i].dateofinterview + '</td>' +
                 '<td>' + data.schedule[i].interviewer + '</td>' +
                 '</tr>');
         }
     })
     $(document).on("click", "#subbut", function() {

         data.Description.push({
             "id": ""+j,
             "title": $("#title").val(),
                "highest degree":$("#de").val(),
                "percentage":$("#pe").val(),
             "experience": $('#exp').val(),
             "openings": $("#open").val(),
             "skills": $("#ski").val()
         });
         j++;
         localStorage.setItem('list', JSON.stringify(data));
     })
     $(document).on("click", ".hmview", function() {
         $("#display").empty();
         $("#display").append('<table class="table tab"><thead class="thead-dark"><tr><th>ID</th><th>NAME</th><th>FATHER NAME</th><th>DATE OF BIRTH</th><th>EMAIL</th><th>PHONE NO</th><th>DEGREE</th>'+
            '<th>SKILLS</th><th>EXPERIENCE</th><th>PERCENTAGE</th></tr></thead><tbody class="tabbody"></tbody></table>');
         for (var i = 0; i < data.applicants.length; i++) {
             $(".tabbody").append('<tr>' +
                 '<td>' + data.applicants[i].appid + '</td>' +
                 '<td>' + data.applicants[i].name + '</td>' +
                 '<td>' + data.applicants[i].fname + '</td>' +
                 '<td>' + data.applicants[i].dob + '</td>' +
                 '<td>' + data.applicants[i].mail + '</td>' +
                 '<td>' + data.applicants[i].phno + '</td>' +
                 '<td>' + data.applicants[i].degree + '</td>' +
                 '<td>' + data.applicants[i].skill + '</td>' +
                 '<td>' + data.applicants[i].experience + '</td>' +
                 '<td>' + data.applicants[i].percentage + '</td>' +
                 '</tr>');
         }
     })
      $(document).on("click", ".sht", function() {
        $("#display").empty();
         $("#display").append('<table class="table tab"><thead class="thead-dark"><tr><th>ID</th><th>NAME</th><th>FATHER NAME</th><th>DATE OF BIRTH</th><th>EMAIL</th><th>PHONE NO</th><th>DEGREE</th>'+
            '<th>SKILLS</th><th>EXPERIENCE</th><th>PERCENTAGE</th><th></th><th></th></tr></thead><tbody class="tabody"></tbody></table>');
         for (var i = 0; i < data.selectlis.length; i++) {
             $(".tabody").append('<tr>' +
                 '<td>' + data.selectlis[i].appid + '</td>' +
                 '<td>' + data.selectlis[i].name + '</td>' +
                 '<td>' + data.selectlis[i].fname + '</td>' +
                 '<td>' + data.selectlis[i].dob + '</td>' +
                 '<td>' + data.selectlis[i].mail + '</td>' +
                 '<td>' + data.selectlis[i].phno + '</td>' +
                 '<td>' + data.selectlis[i].degree + '</td>' +
                 '<td>' + data.selectlis[i].skill + '</td>' +
                 '<td>' + data.selectlis[i].experience + '</td>' +
                 '<td>' + data.selectlis[i].percentage + '</td>' +
                 '<td><input type="text" id="ipt-'+data.selectlis[i].appid+'" size="2" class="ip"></td>'+
                 '<td><button type="button" id="' + data.selectlis[i].appid + '" class="btn btn-info final">final</button></td>' +
                 '</tr>');
         }
     })
      $(document).on("click",".final",function()
      {
        $(this).prop("disabled",true).text("finaled");
        var a=$(this).attr("id");
        console.log(a);
        data.finalist.push({
            "name": data.applicants[a].name,
             "fname": data.applicants[a].fname,
             "appid": data.applicants[a].appid,
             "dob": data.applicants[a].dob,
             "mail": data.applicants[a].mail,
             "phno": data.applicants[a].phno,
             "degree": data.applicants[a].degree,
             "skill": data.applicants[a].skill,
             "experience": data.applicants[a].experience,
             "percentage": data.applicants[a].percentage,
             "marks":$("input"+data.selectlis[a].appid).val()
      })
       localStorage.setItem('list', JSON.stringify(data));
        })
       $(document).on("click",".fin",function()
        { 
            $("#display").empty();
         $("#display").append('<table class="table tab"><thead class="thead-dark"><tr><th>ID</th><th>NAME</th><th>FATHER NAME</th><th>DATE OF BIRTH</th><th>EMAIL</th><th>PHONE NO</th><th>DEGREE</th>'+
            '<th>SKILLS</th><th>EXPERIENCE</th><th>PERCENTAGE</th></tr></thead><tbody class="finbody"></tbody></table>');
         for (var i = 0; i < data.finalist.length; i++) {
             $(".finbody").append('<tr>' +
                 '<td>' + data.finalist[i].appid + '</td>' +
                 '<td>' + data.finalist[i].name + '</td>' +
                 '<td>' + data.finalist[i].fname + '</td>' +
                 '<td>' + data.finalist[i].dob + '</td>' +
                 '<td>' + data.finalist[i].mail + '</td>' +
                 '<td>' + data.finalist[i].phno + '</td>' +
                 '<td>' + data.finalist[i].degree + '</td>' +
                 '<td>' + data.finalist[i].skill + '</td>' +
                 '<td>' + data.finalist[i].experience + '</td>' +
                 '<td>' + data.finalist[i].percentage + '</td>' +
                 '</tr>');
         }
     })

 });