$(document).ready(function() {
    $("#addbut").prop('disabled', true);
    $("#myform").attr('autocomplete', 'off');
    var count = 0,cnt=0;
    var $regmail = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var $regphno = /^([0-9])$/;
    var $regpass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%^&*()_+-=:;""'<>?/\|'])[0-9a-zA-Z~!@#$%^&*()_+-=:;""'<>?/\|']{8,}$/;
    $(document).on("input", 'input,select,textarea', function() {
        if ($(this).val() == '') {
            $(this).addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").removeClass("valid-feedback").text('field is required');
        } else {
            $(this).addClass("is-valid").removeClass("is-invalid").next().addClass("valid-feedback").removeClass("invalid-feedback");
        }
        check();
    });

    function check() {
        $("#myform").find(".is-valid").each(function() {
            count++;
        });
        (count == 9) ? ($("#addbut").prop("disabled", false)) : ($("#addbut").prop("disabled", true));
        count = 0;
    }
    $("#name").on("input", function() {
        var a = $(this);
        if (a.val(a.val().replace(/[0-9_()+_*&^%$#@!~<>?:]/g, ''))) {
            $("#name").addClass("is-valid").next().addClass("valid-feedback").text("");
        }
        check();
    });
    $("#fname").on("input", function() {
        var a = $(this);
        if (a.val(a.val().replace(/[0-9_()+_*&^%$#@!~<>?:]/g, ''))) {
            $("#fname").addClass("is-valid").next().addClass("valid-feedback").text("");
        }
        check();
    });
    $("#appid").on("input", function() {
        var a = $(this);
        if (a.val(a.val().replace(/[a-zA-Z_()+_*&^%$#@!~<>?:]/g, ''))) {
            $("#appid").addClass("is-valid").next().addClass("valid-feedback").text("");
        }
        check();
    });
    $(".email").on("input",function()
  {
  (!$(this).val().match($regmail))?($("#mail").addClass("is-invalid").next().addClass("invalid-feedback").text("enter valid mail")):($("#mail").addClass("is-valid").next().addClass("valid-feedback").text("correct"));
    check();  
  });
    $("#dob").on('change', function() {
        var dateEntered = new Date($("#dob").val());
        var entered = dateEntered.getTime();
        var today = new Date();
        var todayms = today.getTime();
        (entered > todayms) ? ($("#dob").addClass("is-invalid").next().addClass("invalid-feedback").text("enter correct date")) : $(("#dob").addClass("is-valid").next().addClass("valid-feedback"));
        check();
    });
    $("#phno").on("input", function() {
        var a = $(this);
        if (a.val(a.val().replace(/[a-zA-Z_()+_*&^%$#@!~<>?:]/g, ''))) {
            $("#phno").addClass("is-valid").next().addClass("valid-feedback").text("");
        }
        check();
    });
    $("#deg").on("focus click", function() {
        if ($(this).val() == "choose") {
            $("#deg").addClass("is-invalid").next().addClass("valid-feedback").text("enter a value");
        }
    })
    $(".per").on("input", function() {
        console.log($(this).val());
        if ($(this).val() <= 100 && $(this).val() > 0) {
            $(this).addClass("is-valid").removeClass("is-invalid").next().addClass("valid-feedback").text("");
        } else {
            $(this).addClass("is-invalid").removeClass("is-valid").next().addClass("invalid-feedback").text("enter correct value");
        }
    })
    $("#interdate").on("change",function()
    {
      var dateEntered = new Date($("#interdate").val());
        var entered = dateEntered.getTime();
        var today = new Date();
        var todayms = today.getTime();
        (entered < todayms) ? ($("#interdate").addClass("is-invalid").next().addClass("invalid-feedback").text("enter correct date")) : $(("#interdate").addClass("is-valid").next().addClass("valid-feedback"));
        chk();
    })
    $("#intertech").on("focus click",function()
    {
      if ($(this).val() == "choose") {
            $("#intertech").addClass("is-invalid").next().addClass("valid-feedback").text("enter a value");
        }
        else
        {
          $("#intertech").addClass("is-valid");
        }
        chk();
    })


function chk() {
        $("#intform").find(".is-valid").each(function() {
            cnt++;
        });
        (cnt == 2) ? ($("#interbut").prop("disabled", false)) : ($("#interbut").prop("disabled", true));
        cnt = 0;
    }

});